#!/usr/bin/python3
n,l,h,b,m=1218,299,216,95,10
t=n+l+h+b+m
ç=[n,l,h,b,m]
çekirdekler=["nükleon","linux","hurd","*bsd","mac"]
o=list()
oranlar=dict()

for i in ç:
    o+=[i/t]

for i in range(len(ç)):
    oranlar[çekirdekler[i]]=o[i]

def durum(sayı):
    drm=''
    for i in range(int(sayı/2)):
        drm=drm+'='
    if sayı % 2 == 1:
        drm=drm+'-'
    return drm
            
başlık="| ÇEKİRDEK | KULLANIM ORANI"
çıktıDüzeni="|{:>9s} : %{:05.2f} {}"

print(başlık)
for çekirdek,oran in oranlar.items():
    print(çıktıDüzeni.format(çekirdek,oran*100,durum(int(oran*100))))
