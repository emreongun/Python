#!/usr/bin/python3
r"""
'vakit' bir zaman bilgisi veren yazılımdır.
"""
import datetime,locale
locale.setlocale(locale.LC_ALL, '') # yerel dil

şuAn = datetime.datetime.now() # şu anki zaman
buGün = datetime.datetime.strftime(şuAn, '%d %B %Y')

def temizle(arg): # girişten gelen dağınık veriyi düzeltir
    liste = arg.split(' ')
    temizListe = list()
    tarih = ''
    for i in liste:
        if i :
            temizListe.append(i)
            tarih = tarih+str(i)+' '
    tarih = tarih[:-1]
    
    return temizListe,tarih

def yılın_kaçıncı_GH(tarih,GH): # hafta ve gün sırası 
    Yıl = şuAn.year
    dizi,tarih = temizle(tarih)
    çıktı_g = "{} Yılının {}. günü"
    çıktı_h = "{} Yılının {}. haftası."

    try:
        if len(dizi) == 0:
            Gün = datetime.datetime.strftime(şuAn, '%j')
            hafta = datetime.datetime.strftime(şuAn, '%U')
            
        elif len(dizi) == 2:
            T = datetime.datetime.strptime(tarih, '%d %B')
            Gün = datetime.datetime.strftime(T, '%j')
            hafta = datetime.datetime.strftime(T, '%U')
            
        elif len(dizi) == 3: 
            T = datetime.datetime.strptime(tarih, '%d %B %Y')
            Yıl = datetime.datetime.strftime(T, '%Y')
            Gün = datetime.datetime.strftime(T, '%j')
            hafta = datetime.datetime.strftime(T, '%U')

        else:
            raise Exception("Tarihi 'GG AA' ve ya 'GG AA YYYY' biçiminde girin")            

        if GH == 'gün': return çıktı_g.format(Yıl,Gün)
        if GH == 'hafta': return çıktı_h.format(Yıl,hafta)
        
    except UnboundLocalError as hata:
        print("Hata: ",hata)

def yılınGünü(tarih = buGün): return yılın_kaçıncı_GH(tarih,GH = 'gün')
def yılınHaftası(tarih = buGün): return yılın_kaçıncı_GH(tarih,GH = 'hafta')

### TEST ###
DG = "29 Ağustos 1992"

def test_yılınHaftası():
    print(1,yılınHaftası())
    print(2,yılınHaftası(''))
    print(3,yılınHaftası(' '))
    print(4,yılınHaftası(DG))
    print(5,yılınHaftası(" 29  Ağustos "))
    return "tkH"

def test_yılınGünü():
    print(1,yılınGünü())
    print(2,yılınGünü(''))
    print(3,yılınGünü(' '))
    print(4,yılınGünü(DG))
    print(5,yılınGünü(" 29  Ağustos "))
    return "tkG"

#print(test_yılınHaftası(),test_yılınGünü())
