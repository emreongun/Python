#! /usr/bin/python3

AA=4;BA=3.5;BB=3;CB=2.5;CC=2;DC=1.5;DD=1;FF=0

birinci=2*AA+2*BB+3*CB+5*BB+2*AA+2*CB+5*BB; kbir=21
ikinci=2*CB+2*BA+3*CB+1*AA+5*BB+2*BB+2*BA+5*CB; kiki=22
üçüncü=3*CC+1*AA+5*BB+5*CB+3*AA+2*BA+4*CB; küç=23
dördüncü=5*CC+1*BA+5*BB+5*AA+3*AA; kdört=19
beşinci=5*FF+5*BB+5*FF+3*AA; kbeş=18
altıncı=5*BA+1*AA+3*BB+5*FF+5*BA; kaltı=19
yedinci=3*CB+5*BB+5*CC+5*BB+5*AA+3*BB+5*AA+1*AA; kyedi=32

tpuan=birinci+ikinci+üçüncü+dördüncü+beşinci+altıncı+yedinci
tk=kbir+kiki+küç+kdört+kbeş-10+kaltı+kyedi

print("""
    1.dönem ANO:{1:>5.2f}
    2.dönem ANO:{2:>5.2f}
    3.dönem ANO:{3:>5.2f}
    4.dönem ANO:{4:>5.2f}
    5.dönem ANO:{5:>5.2f}
    6.dönem ANO:{6:>5.2f}
    7.dönem ANO:{7:>5.2f}
           AGNO:{0:>5.2f}
""".format(tpuan/tk,
birinci/kbir,ikinci/kiki,üçüncü/küç,dördüncü/kdört,
beşinci/kbeş,altıncı/kaltı,yedinci/kyedi))
