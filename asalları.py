#!/usr/bin/python3
r"""
Verilen sayıları asal çarpanlarına ayıran betik
Kullanımı:
   asalları sayı_1 sayı_2 ...
"""
import sys
import mat # bu dizin içinde olması gereken kütüphane

for i in sys.argv[1:]:
    liste = mat.asalçarpanlar(int(i)) # bu işlev liste döndürür
    print(i,"\tsayısının asal çarpanları: ",liste)
