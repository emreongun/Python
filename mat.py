r"""
Bilgi birikimime dayanarak geliştirdiğim matematik kütüphanesi
Uygulamalarımda bu kütüphane yerine 
3. partiler ile çalışmam daha mantıklı
"""
#!/usr/bin/python3
def fibseri(n):
    seri = list() # = () dilim
    if n==0 or n==1:
        seri.append(1)
        return seri
    else:
        a,b=0,1
        for i in range(n):
            seri.append(a)
            a,b=b,a+b
        return seri

def fib(n):
    seri=fibseri(n)
    return seri[len(seri)-1]

def asalçarpanlar(n):
    çarpanlar = [] # asal çarpanlar bu listeye eklenecek
    x = 2
    while n > 1:
        asal = True
        i=2
        while i*i <= x:
            if x % i == 0:
                asal = False
                break
            i += 1

        if asal and n % x == 0:
            çarpanlar.append(x)
            while n % x == 0:
                n = n/x
        x += 1

    return çarpanlar


import math   # karekök için
def stsapma(x1,x2, *y):
    N = len(y) + 2 # kaç tane sayı olduğu

    # ortalamayı hesapla
    toplam = x1 + x2
    for z in y:
        toplam += z
    ort = 1.0 * toplam / N

    karetoplam = (x1-ort)**2 + (x2-ort)**2
    for z in y:
        karetoplam += (z-ort)**2
    stsap = math.sqrt(1.0 * karetoplam / (N-1))

    return ort, stsap
