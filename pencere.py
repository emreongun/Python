#!/usr/bin/python3
import tkinter as tk

class Pencere(tk.Tk):
    def __init__(self):
        super().__init__()
        # çıkış tuşuna basıldığında self.çıkış işlevi çağırılır.
        self.protocol('WM_DELETE_WINDOW', self.çıkış)

        self.etiket = tk.Label(text='Pyton-Tkinter Pencere')
        self.etiket.pack()

        self.düğme = tk.Button(text='Çık', command=self.çıkış)
        self.düğme.pack()

    def çıkış(self):
        self.etiket['text'] = 'Pencere kapanıyor...'
        self.düğme['text'] = 'Bekleyin...'
        self.düğme['state'] = 'disabled'
        self.after(2000, self.destroy)

pencere = Pencere()
pencere.mainloop()
