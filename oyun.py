#!/usr/bin/python3
import time
import random
import sys

class Oyuncu():
    def __init__(self, isim="bilgisayar", can=5, enerji=60):
        self.isim = isim
        self.darbe = 0
        self.can = can
        self.enerji = enerji

    @staticmethod
    def durumÇubuğu(sayı): # durumu çubuk şeklinde gösterir
        drm=''
        for i in range(int(sayı/2)):
            drm=drm+'='
        if sayı % 2 == 1:
            drm=drm+'-'
        return sayı,drm
    
    def mevcut_durumu_görüntüle(self):
        print('isim  :',self.isim)
        a,b=self.durumÇubuğu(self.darbe)
        print('darbe : {:2} {}'.format(a,b))
        a,b=self.durumÇubuğu(self.can)
        print('can   : {:2} {}'.format(a,b))
        a,b=self.durumÇubuğu(self.enerji)
        print('enerji: {:2} {}'.format(a,b))
        print()
        
    def darbele(self, darbelenen):
        darbelenen.darbe += 10
        darbelenen.enerji -= 10
        if (darbelenen.enerji % 5) == 0:
            darbelenen.can -= 1
        if darbelenen.can < 1:
            darbelenen.enerji = 0
            self.mevcut_durumu_görüntüle()
            darbelenen.mevcut_durumu_görüntüle()
            print('Oyunu {} kazandı!'.format(self.isim))
            self.oyundan_çık()

    def oyundan_çık(self):
        print('Çıkılıyor...')
        sys.exit()
        
    def saldırı_sonucunu_hesapla(self):
         return random.randint(0, 2)

    @staticmethod
    def işlem_yapılıyor(karakter): # işlem yaıldığını gösteren çizgi
        for i in range(40):
            sys.stdout.write(karakter)
            sys.stdout.flush()
            time.sleep(.3)
        print()
        
    def saldır(self, rakip):
        print('Bir saldırı gerçekleştirdiniz.')
        print('Saldırı sürüyor. Bekleyiniz.')
        self.işlem_yapılıyor('#')

        sonuç = self.saldırı_sonucunu_hesapla()

        if sonuç == 0:
            print('SONUÇ: kazanan taraf yok\n')

        if sonuç == 1:
            print('SONUÇ: rakibinizi darbelediniz\n')
            self.darbele(rakip)

        if sonuç == 2:
            print('SONUÇ: rakibinizden darbe aldınız\n')
            self.darbele(self)

    def kaç(self):
        print('Kaçılıyor...')
        self.işlem_yapılıyor('*')
        print('Rakibiniz sizi yakaladı\n')


##################################

# Oyuncular
siz = Oyuncu('oyuncu',10,100)
rakip = Oyuncu()

# Oyun başlangıcı
while True:
    print('Şu anda rakibinizle karşı karşıyasınız.',
          'Yapmak istediğiniz hamle: ',
          'Saldır:  s',
          'Kaç:     k',
          'Çık:     q', sep='\n')

    hamle = input('\n> ')
    if hamle == 's':
        siz.saldır(rakip)

        #print('Rakibinizin durumu')
        rakip.mevcut_durumu_görüntüle()
        print()
        #print('Sizin durumunuz')
        siz.mevcut_durumu_görüntüle()

    if hamle == 'k':
        siz.kaç()

    if hamle == 'q':
        siz.oyundan_çık()
