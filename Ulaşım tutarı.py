#! /usr/bin/python3
# -*- coding: utf-8 -*-
# <nbformat>3.0</nbformat>

# <codecell>

yıl=365.6
haftalık_hareket=5/7
ay=12
tarife=1.10
aktarma=0.45

yıllık_masraf=2*yıl*haftalık_hareket*tarife
aylık_masraf=yıllık_masraf/ay
yıllık_yüksek_masraf=yıllık_masraf/tarife*(tarife+aktarma)
aylık_yüksek_masraf=yıllık_yüksek_masraf/12

metin="""
Aylık azami yol masrafı:\t{aylık:<4.2f} TL
Yıllık azami yol masrafı:\t{yıllık:<4.2f} TL
Aylık yüksek yol masrafı:\t{Aylık:<4.2f} TL
Yıllık yüksek yol masrafı:\t{Yıllık:<4.2f} TL\n"""

print(metin.format(aylık=aylık_masraf,yıllık=yıllık_masraf,Aylık=aylık_yüksek_masraf,Yıllık=yıllık_yüksek_masraf))
             

