#!/usr/bin/python3
import sqlite3 as sql

# yoksa veritabanı oluşturur varsa bağlanır
vt = sql.connect('oyun.sqlite')

im = vt.cursor() # imleç oluşturur

sorgu = """CREATE TABLE IF NOT EXISTS Oyuncular (isim, soyisim, taraf)"""
değer_gir = """INSERT INTO Oyuncular VALUES (?,?,?)"""
Oyuncular=[('Emre','ONGUNG','elfler'),
          ('Erdem','ONGUNG','yarı ölüler')]

im.execute(sorgu)
for veri in Oyuncular:
    im.execute(değer_gir,veri)
    
vt.commit() # veritabanına işlemek için
vt.close()
